
//==========//
// Wavelets //
//==========//

//===============//
// code --> 0004 //
//===============//

// keywords: Array, wavelet, LiftingStep, DUAL, PRIMAL

#include <blitz/array.h>
#include <Wavelet.h>
#include <arrayTools.h>
#include <WaveletDecomp.h>

// namespaces

BZ_USING_NAMESPACE(blitz)

using namespace bwave;

// the main function

int main()
{
	// --> a local hand-made wavelet

	Wavelet myWavelet("MyWavelet", 
		  	  sqrt(2.0), 
		          sqrt(2.0)/2,
        		  Wavelet::LiftingStep(Wavelet::LiftingStep::DUAL, 0,  2, -1, -1),
        		  Wavelet::LiftingStep(Wavelet::LiftingStep::PRIMAL, -1,  4,  1,  1));

	// --> output the declared wavelets

	cout << " --> myWavelet = " << myWavelet << endl;	

	// --> some predefined wavelets

	// 1

	// The Cohen-Daubechies-Feauveau (CDF) (1,1) wavelet,
	// identical to the Haar wavelet

        // Wavelet WL_CDF_1_1;

	cout << " --> WL_CDF_1_1 = " << WL_CDF_1_1 << endl;

	// 2

	// The CDF(2,2) biorthogonal wavelet
	// This wavelet (also known as LeGall(5,3)) is used in
	// the JPEG2000 standard

	// Wavelet WL_CDF_2_2;

	cout << " --> WL_CDF_2_2 = " << WL_CDF_2_2 << endl;

	// 3
	
	// The CDF(3,1) biorthogonal wavelet

	// Wavelet WL_CDF_3_1;

	cout << " --> WL_CDF_3_1 = " << WL_CDF_3_1 << endl;

	// 4

	// The CDF(3,3) biorthogonal wavelet

	// Wavelet WL_CDF_3_3;

	cout << " --> WL_CDF_3_3 = " << WL_CDF_3_3 << endl;
	
	// 5

	// The CDF(4,2) biorthogonal wavelet

	// Wavelet WL_CDF_4_2;

	cout << " --> WL_CDF_4_2 = " << WL_CDF_4_2 << endl;

	// 6

	// The CDF(9,7) biorthogonal wavelet (used in JPEG 2000)
	// This wavelet (often called Daubechies(9,7) or just 9/7-filter
	// in the literature) is used in the JPEG2000 standard

	// Wavelet WL_CDF_97;

	cout << " --> WL_CDF_97 = " << WL_CDF_97 << endl;

	// 7
	
	// The Haar wavelet

	// Wavelet WL_HAAR;

	cout << " --> WL_HAAR = " << WL_HAAR << endl;

	// 8

	// The LeGall(5,3) biorthogonal wavelet, identical to CDF(2,2)
	// This wavelet is used in the JPEG2000 standard

	// Wavelet WL_LEG_5_3;

	cout << " --> WL_LEG_5_3 = " << WL_LEG_5_3 << endl;

	// 9

	// A cubic-spline biorthogonal wavelet, equals CDF(4,2) except
	// for normalization
	// Lifting includes a scaling step leading to information loss for
	// integer values during recomposition

	// Wavelet WL_CUBIC_SPLINE;

	cout << " --> WL_CUBIC_SPLINE = " << WL_CUBIC_SPLINE << endl;

	// 10

	// The Daubechies D4 wavelet

	// Wavelet WL_D_4;

	cout << " --> WL_D_4 = " << WL_D_4 << endl;

	// marks the end

	cout << " --> end" << endl;

	// sentineling

	int sentinel;
	cin >> sentinel;

	cout << " --> exit" << endl;

    	return 0;
}

//======//
// FINI //
//======//

