
//=================//
// array slow-down //
//=================//

// the reason for the slow-down is the following:
// although the number of element are being set o zero are the same each time
// their locations in RAM are being changed. As the stride gets larger
// the positions are far away from each other and the systems needs to
// fetch the new element to caches. They are not in the caches.
// Only consequtive elements of the array are in the caches.

#include <iostream>
#include <ctime>

using std::cout;
using std::cin;
using std::endl;

// the main function

int main() 
{
	for (int e = 2; e <= 22; ++e)
	{
		for (int stride = (1<<e)-3; stride <= (1<<e)+3; ++stride)
		{
			int n = 345;
	
			int * data = new int[n*stride];

			double timer = clock();

			for (int dummy = 0; dummy < 100000; ++dummy)
			{
				for (int i = 0; i < n; ++i) // here is the slow-down
				{
					data[stride*i] = 0; // here is the slow-down
				}			    // because of the bigger each time step
			}
			
			cout << "n: " 
                             << n 
                             << "\tstride: " 
                             << stride
			     << "\ttime: " 
                             << (clock() - timer)/100000 
                             << endl;

			delete [] data;
		}
		
		cout << endl;
	}

	return 0;
}

//======//
// FINI //
//======//

