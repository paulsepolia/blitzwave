
//======================//
// very first test code //
//======================//

//===============//
// code --> 0001 //
//===============//

// keywords: Array, sum, pow2, sin, cos, exp, max, min

#include <blitz/array.h>
#include <Wavelet.h>
#include <arrayTools.h>
#include <WaveletDecomp.h>

BZ_USING_NAMESPACE(blitz)

using namespace bwave;

// the main function

int main()
{
	const int I_MAX = 10000;
    	const int N = 7000;

    	Array<double,2> A(N,N);
    	Array<double,2> B(N,N);
    	Array<double,2> C(N,N);
    	Array<double,2> D(N,N);
    	Array<double,2> E(N,N);
    
	A = 5.0;
    	B = 0.0;
    	C = 0.0;

	Wavelet myWavelet( "CDF(2,2)", sqrt(2.0), sqrt(2.0)/2,
        		   Wavelet::LiftingStep(Wavelet::LiftingStep::DUAL, 0,  2, -1, -1),
        		   Wavelet::LiftingStep(Wavelet::LiftingStep::PRIMAL, -1,  4,  1,  1));


    	for (int i = 0; i != I_MAX; i++)
    	{
		cout << "------------------------------------------------------->> " << i << endl;

        	D = A + B + C;

        	D /= sum(pow2(D));

        	A = B * cos(D) + C * sin(D);

        	B += exp(-D);

        	double x = sum(A);

        	double y = sum(A+B);

        	double z = sum(sqr(A)+sqr(B));

        	C = x*A+y*B+z*C;

        	D = exp(-sqr(A)-sqr(B));

        	E = A + B + C + D;

        	double q = min(A);

        	double r = max(B);

		cout << " --> x = " << x << endl;
		cout << " --> y = " << y << endl;
		cout << " --> z = " << z << endl;
		cout << " --> q = " << q << endl;
		cout << " --> r = " << r << endl;

		cout << " --> sining 5 matrices ... wait" << endl;
	
		A = sin(A);
		B = sin(B);
		C = sin(C);
		D = sin(D);
		E = sin(E);
	
		cout << " --> okay..." << endl;
    	}

	cout << " --> end" << endl;

	// sentineling

	int sentinel;
	cin >> sentinel;

	cout << " --> exit" << endl;

    	return 0;
}

//======//
// FINI //
//======//

