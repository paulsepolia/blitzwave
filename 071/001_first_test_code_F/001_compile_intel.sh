#!/bin/bash

  # 1. compile

  icpc -O3                                         \
       -xHost                                      \
       -Wall                                       \
       -std=c++11                                  \
       -static                                     \
       -wd2012                                     \
       driver_program.cpp                          \
       /opt/blitzwave/071/intel/lib/libblitzwave.a \
       -o x_intel
