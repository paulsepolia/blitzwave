
//=========//
// Wavelet //
//=========//

//===============//
// code --> 0003 //
//===============//

// keywords: Array, wavelet, CDF(2,2), LiftingStep, DUAL, PRIMAL

#include <blitz/array.h>
#include <Wavelet.h>
#include <arrayTools.h>
#include <WaveletDecomp.h>

// namespaces

BZ_USING_NAMESPACE(blitz)

using namespace bwave;

// the main function

int main()
{
	// a wavelet

	Wavelet myWavelet("CDF(2,2)", 
			  sqrt(2.0), 
			  sqrt(2.0)/2,
        		  Wavelet::LiftingStep(Wavelet::LiftingStep::DUAL, 0,  2, -1, -1),
        		  Wavelet::LiftingStep(Wavelet::LiftingStep::PRIMAL, -1,  4,  1,  1));

	// output a wavelet

	cout << " --> myWavelet = " << myWavelet << endl;	


	// mark the end

	cout << " --> end" << endl;

	// sentineling

	int sentinel;
	cin >> sentinel;

	cout << " --> exit" << endl;

    	return 0;
}

//======//
// FINI //
//======//

