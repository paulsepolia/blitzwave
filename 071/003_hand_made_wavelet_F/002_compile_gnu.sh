#!/bin/bash

  # 1. compile

  g++-4.8 -O3                \
          -Wall              \
          -std=c++0x         \
          -static            \
          driver_program.cpp \
          /opt/blitzwave/071/gnu_480/lib/libblitzwave.a \
          -o x_gnu
